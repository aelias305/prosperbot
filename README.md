# README #

Automate the investment in prosper.com notes using prosper.com api

### How do I get set up? ###

# Summary of set up #

Code Defaults to $25 per loan.  Change if necessary

Code must have distribution allocation based on propser rating (AA,A,B,C,D,E).  Distribution must add up to 100% in the code.

Confirm at the top of the codebase what this allocation is
* investDist = {'AA': 0.03, 'A' : 0.47, 'B':0.48, 'C': 0.02}

Code will check every 15 seconds for new loans as long as it is running.

# Configuration #

Setup Prosper and get the client ID and Secret Key.  Instructions here for an individual investor

https://developers.prosper.com/docs/authenticating-with-oauth-2-0/password-flow/

To configure the application do the following:

create a .env files to put your passwords in  The file should have the following

* username = "<Prosper username>"
* password = "<Prosper Password>"
* clientid = "<Prosper Secret Client ID>"
* clientSecret = "<Prosper Secret Key>"

# If you would like to send emails when a order is submitted... works with gmail #
Gmail requires you allow "less secure access" which is not recommended.  Use your own smtp server is you can
Leave these out of the .env file and it will not attempt to send any emails

* gmail_user = "<Gmail User Name>"
* gmail_password = "<gmail password>"

#  Twilio SMS Text when an order is created #
If you have a twillio account and would like a text message sent to you.  It will send it each time an order is submitted as well as once a day at 9 AM (to tell you the application is still running)

## To install twilio client:
** pip install twilio **

## Twilio Credentials
* twilio_account_sid = '<twilio_account_sid>'
* twilio_auth_token = '<twilio_auth_token>'
* twilio_from_phone = '+<twilio_from_phone>'
* twilio_to_phone = '+<twilio_to_phone>'




# Dependencies #

Python3 and the following python libraries check the code for the libraries required.
* from datetime import datetime
* from decouple import config
* from twilio.rest import Client

* import requests
* import json
* import logging
* import smtplib
* import traceback
* import time
* import sys


# Execution instructions #

python3 prosperbot.py

