from datetime import datetime
from decouple import config

import requests
import json
import logging
import smtplib
import traceback

properbaseurl = "https://api.prosper.com"

accountsURL = properbaseurl+"/v1/accounts/prosper/"

authurl = properbaseurl + "/v1/security/oauth/token"
authheader = { 'accept': "application/json", 'content-type': "application/x-www-form-urlencoded" }
apiheader = {}
postheader = {}
paymentsheader = {}
expireSeconds = -1
lastChecked = datetime.now()

username = config('username', default='')
password = config('password', default='')
clientid = config('clientid', default='')
clientSecret = config('clientSecret', default='')
gmail_user = config('gmail_user', default='')
gmail_password = config('gmail_password', default='')

accessToken = ""
refreshToken = ""


def __tokenRequest():
	global accessToken
	global refreshToken
	global apiheader
	global expireSeconds
	global lastChecked
	global postheader
	global paymentsheader
	payload = "grant_type=password&username="+username+"&password="+password+"&client_id="+clientid+"&client_secret="+clientSecret
	
	response = requests.request("POST", authurl, data=payload, headers=authheader).json()
	#resData = json.load(response.text)
	accessToken = response['access_token']
	refreshToken = response['refresh_token']
	expireSeconds = response['expires_in']
	lastChecked = datetime.now()
	apiheader = { 'accept': "application/json", 'content-type': "application/x-www-form-urlencoded", 'Authorization' : "bearer "+accessToken, 'timezone': "America/Denver" }
	postheader = { 'Accept': "application/json", 'Content-Type': "application/json", 'Authorization' : "bearer "+accessToken}
	paymentsheader = { 'Accept': "application/json", 'Authorization' : "bearer "+accessToken, 'timezone': "America/Denver"}
	logging.debug(response)
	logging.debug(accessToken)


def __tokenRefresh():
	payload = "grant_type=refresh_token&client_id="+clientid+"&client_secret="+clientSecret+"&refresh_token="+refreshToken
	response = requests.request("POST", authurl, data=payload, headers=authheader)
	logging.info("Refeshing Access Token")
	logging.debug(response.text)


def getApiHeader():
	nowdt = datetime.now()
	seconds = nowdt - lastChecked
	if(seconds.seconds > expireSeconds ):
		logging.info("Requesting New Token")
		__tokenRequest()
	return apiheader

def getPostHeader():
	getApiHeader()
	return postheader

def getPaymentsHeader():
	getApiHeader()
	return paymentsheader


def sendemail(emailbody):
	if(gmail_user == ''):
		logging.debug("Nothing to email.")
		return

	sent_from = gmail_user
	to = [gmail_user]
	subject = 'Order Submitted'


	email_text = """\
	From: %s
	To: %s
	Subject: %s

	%s
	""" % (sent_from, ", ".join(to), subject, emailbody)

	try:
	    smtp_server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
	    smtp_server.ehlo()
	    smtp_server.login(gmail_user, gmail_password)
	    smtp_server.sendmail(sent_from, to, "Subject: {}\n\n{}".format(subject,emailbody))
	    smtp_server.close()
	    logging.info ("Email sent successfully!")
	except Exception as ex:
		logging.error(traceback.format_exc())




	#print(message.sid)


#sendemail("This is a test email")

