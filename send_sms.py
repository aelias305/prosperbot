# Download the helper library from https://www.twilio.com/docs/python/install
from twilio.rest import Client
from decouple import config
from prosperStats import NoteStats

import traceback
import logging
import time
import prosperStats
import re

twilio_account_sid = config('twilio_account_sid', default='')
twilio_auth_token = config('twilio_auth_token', default='')
twilio_from_phone = config('twilio_from_phone', default='')
twilio_to_phone = config('twilio_to_phone', default='')

last_keep_alive = float('0')


def sendSMS(body, to_phone_number = twilio_to_phone):
     if twilio_account_sid == '' or twilio_auth_token == '' or twilio_from_phone == '' or twilio_to_phone == '':
          logging.debug("SMS Not Setup.  Nothing to text.")
          ## Do Nothing.  Twilio is not setup
          return
     # Find your Account SID and Auth Token at twilio.com/console
     # and set the environment variables. See http://twil.io/secure   
     try:
          client = Client(twilio_account_sid, twilio_auth_token)

          message = client.messages \
                .create(
                     body=body,
                     from_= twilio_from_phone,
                     to=to_phone_number
                 )
          logging.debug("SMS Sent: " + body)
     except Exception as ex:
          logging.error(traceback.format_exc())

def dailyCheckin(loanStats):
     global last_keep_alive
     now = time.time()
     ## If 12 hours has past since last keep alive text was sent and it's between 9AM and 10AM, go ahead and send a keep alive text
     if now - last_keep_alive > (24*60*60):
          ## 12 hours has passed -- now check if it's between 9AM and 10 AM
          if time.localtime().tm_hour == 9:
               msg = "Prosper Bot is still alive! (^_^) \n\n" \
                    + "I am invested in: "+str(loanStats.getTotalActiveNotes())+" notes\n" \
                    + "Avg yield of "+str(loanStats.getAverageYeild())

               msg += "%\nMTD Pmts Rcvd:$"+'{0:.2f}'.format(loanStats.getMTDPaymentsDeposited())
               if(loanStats.isLastDayOfMonth()):
                    msg += "\n\n***Please withdraw above amount today!***"
               sendSMS( msg )
               last_keep_alive = time.time()

def validateSMS(phoneNumber):
     ## Validate Phone
     validate = re.sub("[^0-9]", "", phoneNumber)
     if len(validate) != 11 :
          return False, "Must be 11 digits long"
     if validate[0] != "1" :
          return False, "Must start with a 1"

     validate = "+" + validate
     sendSMS("ProsperBot Text Msg Validation (^_^)", phoneNumber)
     return True, ""






#checkKeepAlive()


#sendSMS("Test Me Out!")