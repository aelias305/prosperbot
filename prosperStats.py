import prosperAuth
import requests
import json
import logging
import time
import traceback
import datetime
from datetime import datetime, timedelta



properbaseurl = "https://api.prosper.com"

last_stat_check = float('0')



class NoteStats:
	def __init__(self):
		self.active_listings = set()
		self.active_loan_numbers = set()
		self.active_notes = 0
		self.total_yeild = 0
		self.MTDPaymentsDeposited = 0.0
		
	def __str__(self):
		return "Total Notes: " + str(self.active_notes) + "\nPayments Collected: $" + str(self.MTDPaymentsDeposited) + "\nAverage Yeild: "+str(self.getAverageYeild())+"%"


	def getAverageYeild(self):
		return round(self.total_yeild / self.active_notes * 100, 2)

	def getTotalActiveNotes(self):
		return len(self.active_listings)

	def addListing(self,listing):
		self.active_listings.add(listing)

	def addLoanNumber(self, loannumber):
		self.active_loan_numbers.add(loannumber)
		self.active_notes += 1

	def getActiveListings(self):
		return self.active_listings

	def getActiveLoanNumbers(self):
		return self.active_loan_numbers

	def addActiveYeild(self, yeild):
		self.total_yeild += yeild

	def addPaymentDeposited(self,payment):
		self.MTDPaymentsDeposited += payment

	def getMTDPaymentsDeposited(self):
		return self.MTDPaymentsDeposited

	def isLastDayOfMonth(self):
		#return True
		return check_if_last_day_of_month()


loanStatCache = NoteStats()

## Retrieve Listing Numbers with status of:
##   1 - Current
##   2 - Charge Off (Loans in recovery status)
## 
def getActiveListingStats():
	loanStats = NoteStats()
	limit = 100
	notesURL = properbaseurl+"/v1/notes/?limit="+str(limit)
	listings = requests.request("GET", notesURL, headers = prosperAuth.getApiHeader()).json()
	#print(json.dumps(listings, indent=2))
	total_results = listings['total_count']
	this_result_count = listings['result_count']
	logging.debug(this_result_count)
	gatherLoanNumbers(this_result_count, listings, loanStats)

	for x in range(this_result_count, total_results, limit):
		listings = requests.request("GET", notesURL+"&offset="+str(x), headers = prosperAuth.getApiHeader()).json()
		#print(json.dumps(listings, indent=2))
		total_results = listings['total_count']
		this_result_count = listings['result_count']
		gatherLoanNumbers(this_result_count, listings, loanStats)
		#print(this_result_count)
	
	return loanStats

def gatherLoanNumbers(this_result_count, listings, loanStats):
	
	for y in range(this_result_count):
		loanStats.addListing(listings['result'][y]['listing_number'])
		#print(str(listings['result'][y]['interest_paid_pro_rata_share']))
		if listings['result'][y]['note_status'] == 1: 
			loanStats.addLoanNumber(listings['result'][y]['loan_number'])


def getTotalMonthToDatePaymentsReceivedForlistings(listing_numbers, loanStats):
	if len(listing_numbers) == 0:
		return

	firstDayOfMonth = datetime.today().replace(day=1)

	paymentsURL = properbaseurl+"/v1/loans/payments/?limit=100&loan_number="+listing_numbers
	payments = requests.request("GET", paymentsURL, headers = prosperAuth.getPaymentsHeader()).json()
	logging.debug(json.dumps(payments, indent=2))
	total_results = payments['total_count']
	this_result_count = payments['result_count']
	for x in range(this_result_count):
		## If funds_available_date is in current month, add it to the total payments recieved
		if 'funds_available_date' in payments['result'][x] :
			fundsAvailableDate = payments['result'][x]['funds_available_date']
			dt = datetime.strptime(fundsAvailableDate[0:fundsAvailableDate.find('T')], '%Y-%m-%d')
		#dt = datetime.strptime("2022-01-01", '%Y-%m-%d')
		
			logging.debug(str(firstDayOfMonth) + " Comparing to "+str(dt))
			if firstDayOfMonth.month == dt.month and firstDayOfMonth.year == dt.year:
				logging.debug("This is in the current month!")
				loanStats.addPaymentDeposited(payments['result'][x]['payment_amount'])
	



## Retreive Payments for all active listings.  Payments only go back the last 90 days.  We are only intereted in payments which have
## posted month to date of the current month.
def getTotalMonthToDatePaymentsReceived(loanStats):
	limit = 100
	loan_number_limit = 20

	logging.debug("Get payments for the following listing nummbers")
	logging.info("Found Active Loans:"+str(loanStats.getActiveLoanNumbers()))
	## you can only get 20 loans at a time  So we need to query 20 loans, get the payments the qualify and the query the next 20
	loanCount = 0
	listing_numbers = ""
	for listingNumber in loanStats.getActiveLoanNumbers():
		loanCount += 1
		if len(listing_numbers) > 0: listing_numbers += ","
		listing_numbers += str(listingNumber)
		if(loanCount >= loan_number_limit):
			loanCount = 0
			getTotalMonthToDatePaymentsReceivedForlistings(listing_numbers, loanStats)
			listing_numbers = ""

	if len(listing_numbers) > 0: 
		getTotalMonthToDatePaymentsReceivedForlistings(listing_numbers, loanStats)

def calcLoanStats(loanStats):
	loanUrl = properbaseurl+"/v1/loans/"

	## Calculate Average Yeild for Notes
	for loanid in loanStats.getActiveLoanNumbers():
		loan = requests.request("GET", loanUrl+str(loanid), headers = prosperAuth.getPaymentsHeader()).json()
		loanStats.addActiveYeild(loan['borrower_rate'])

	getTotalMonthToDatePaymentsReceived(loanStats)

def check_if_last_day_of_month():
	to_date = datetime.now()
	delta = timedelta(days=1)
	next_day = datetime.now() + delta
	if to_date.month != next_day.month:
		return True
	return False

def getLoanStats():
	global last_stat_check
	global loanStatCache

	# Lets check if we need to update our loan stats
	right_now = time.time()
    # Refresh Loan Stats every 4 hours
	if right_now - last_stat_check > (4*60*60):
		logging.info("Refeshing Loan Stats")
		try:
			loanStats = getActiveListingStats()
			calcLoanStats(loanStats)
			loanStatCache = loanStats
			logging.info(str(loanStatCache))
			last_stat_check = time.time()
		except Exception as ex:
			logging.error(traceback.format_exc())
	
	
	return loanStatCache

#print(check_if_last_day_of_month())

#loanStats = getLoanStats()
#print(loanStats.getAverageYeild())
#print(loanStats.getActiveLoanNumbers())
#print(loanStats.getActiveListings())
#print(loanStats.getMTDPaymentsDeposited())
#print("total active notes = "+str(loanStats.getTotalActiveNotes()))




